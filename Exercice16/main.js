var dernierSalaire, anciennete, age, indemnite;
var prompt = require("prompt-sync")();
var dernierSalaire = prompt("Saisir votre votre dernier salaire : ");
var age = prompt("Saisir votre âge : ");
var anciennete = prompt("Saisir votre nombre d'années d'ancienneté : ");

function calcul(age,dernierSalaire,anciennete) {

if (anciennete>10) {
    indemnite = dernierSalaire*anciennete;
} else {
    indemnite = (dernierSalaire/2)*anciennete;
}

if (age<49) {
    if (age<46) {
        console.log("Pas d'indemnités supplémentaires en rapport avec l'âge.")
    } else {
        indemnite = indemnite+(dernierSalaire*2);
    }
} else {
    indemnite = indemnite+(dernierSalaire*5);
}

console.log("Le montant d'indemnités pour un salarié de "+age+" ans et avec "+anciennete+" années d'ancienneté s'élève à "+indemnite)
}

calcul(age,dernierSalaire,anciennete)