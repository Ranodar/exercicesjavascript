var val=0;
function augmentation() {
    let valeur=document.getElementById("valeur");
    val++;
    valeur.textContent=val;
    if (val%3==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="Buzz";
        document.getElementById("valeur").classList.remove("text-warning");
        document.getElementById("valeur").classList.remove("text-success");
        document.getElementById("valeur").classList.add("text-primary");
    }else{
        document.getElementById("valeur").classList.remove("text-primary");
    }
    if (val%5==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="Fizz";
        document.getElementById("valeur").classList.remove("text-warning");
        document.getElementById("valeur").classList.remove("text-primary");
        document.getElementById("valeur").classList.add("text-success");
    }else{
        document.getElementById("valeur").classList.remove("text-success");
    }
    if (val%3==0&&val%5==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="FizzBuzz";
        document.getElementById("valeur").classList.remove("text-primary");
        document.getElementById("valeur").classList.remove("text-success");
        document.getElementById("valeur").classList.add("text-warning");
    }else{
        document.getElementById("valeur").classList.remove("text-warning");
    }
}
function diminution() {
    let valeur=document.getElementById("valeur");
    val=val-1;
    valeur.textContent=val;
    if (val%3==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="Buzz";
        document.getElementById("valeur").classList.remove("text-warning");
        document.getElementById("valeur").classList.remove("text-success");
        document.getElementById("valeur").classList.add("text-primary");
    }else{
        document.getElementById("valeur").classList.remove("text-primary");
    }
    if (val%5==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="Fizz";
        document.getElementById("valeur").classList.remove("text-warning");
        document.getElementById("valeur").classList.remove("text-primary");
        document.getElementById("valeur").classList.add("text-success");
    }else{
        document.getElementById("valeur").classList.remove("text-success");
    }
    if (val%3==0&&val%5==0) {
        let valeur=document.getElementById("valeur");
        valeur.textContent="FizzBuzz";
        document.getElementById("valeur").classList.remove("text-primary");
        document.getElementById("valeur").classList.remove("text-success");
        document.getElementById("valeur").classList.add("text-warning");
    }else{
        document.getElementById("valeur").classList.remove("text-warning");
    }
}

var nbCoups2=0;
var nbGagnant=Math.floor(Math.random()*50)+1;

function essai() {
    let nbRentre=parseInt(document.getElementById("nbRentre").value);
    console.log(nbRentre);
    let nbCoups=document.getElementById("nbCoups");
    let aide=document.getElementById("aide");
    if (nbRentre>nbGagnant) {
            aide.textContent="Plus petit";
            nbCoups2++;
        } else if (nbRentre<nbGagnant) {
            aide.textContent="Plus grand";
            nbCoups2++;
        } else if (nbRentre==nbGagnant) {
            aide.textContent="Trouvé";
            nbCoups2=0;
        } else {
            aide.textContent="Ce n'est pas un nombre";
        }
    if (nbCoups2>4) {
        aide.textContent="5ième essai, vous avez perdu !";
        rejouer();
    }
    nbCoups.textContent=nbCoups2;
}

function rejouer() {
    let nbCoups=document.getElementById("nbCoups");
    nbCoups2=0;
    nbGagnant=Math.floor(Math.random()*50)+1;
    nbCoups.textContent=nbCoups2;
    document.getElementById("aide").classList.remove.textContent;
}