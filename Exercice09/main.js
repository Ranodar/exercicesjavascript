var capital, taux, duree;

var prompt = require("prompt-sync")();
var capital = prompt("Saisir le capital de départ : ");
var taux = prompt("Saisir le taux d'intérêt : ");
var duree = prompt("Saisir la durée de l'épargne : ");
console.log("Avec un capital initial de "+capital+"€, placé à "+taux+"% pendant "+duree+" année(s)");
console.log("Le montant total des intérêts s'élèvera à "+(Math.round((capital*Math.pow((1+(taux/100)),duree)))-capital)+"€");
console.log("Le capital final à l'issue sera de "+Math.round((capital*Math.pow((1+(taux/100)),duree)))+"€");