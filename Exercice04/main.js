var mot, inverse;

var prompt = require("prompt-sync")();
var mot = prompt("Saisir un mot : ");
function reverseInPlace(str) {
    var words = [];
    words = str.match(/\S+/g);
    var result = "";
    for (var i = 0; i < words.length; i++) {
       result += words[i].split('').reverse().join('') + " ";
    }
    return result
  }
//console.log(reverseInPlace("abd fhe kdj"))
var inverse = reverseInPlace(mot);
if(mot == inverse){
    console.log("Le mot "+mot+" est un palindrome");
}else{
    console.log("Le mot "+mot+" n'est pas un palindrome");
}