var diametre;

var prompt = require("prompt-sync")();
var diametre = prompt("Saisir le diamètre : ");

console.log("Périmètre = "+ Math.PI*diametre);
console.log("Aire = "+ Math.PI*((diametre/2)*(diametre/2)));
console.log("Périmètre (arrondi) = "+ Math.round(Math.PI*diametre));
console.log("Aire (arrondi) = "+ Math.round(Math.PI*((diametre/2)*(diametre/2))));